/*
 * main.c
 *
 *  Created on: 09.11.2018
 *      Author: user
 */
#include<avr/io.h>
#include<util/delay.h>
#include<avr/interrupt.h>



#define CZAS 600 // czas w sekundach (10min)

#define PRZEK PD7
#define IGBT PB0


volatile uint32_t czas = 1;

int main()
{

	TCCR1B |=  (1<<WGM12) ;
	TCCR1B |= (1<<CS12) | (1<<CS10) ; // PRESDKLALER 1024 ;

	OCR1A = 15625; // 1 SEKUNDA
	TIMSK |= (1<<OCIE1A);


	TCCR2 |= (1<<WGM21); // CTC
	OCR2 = 165 ;
	TIMSK |= (1<<OCIE2);





	DDRD |= (1<<PRZEK);
	DDRB |= (1<<PB1);
	DDRB |= (1<<IGBT);

	PORTD |= (1<<PRZEK);

	sei();
	while(1)
	{
		PORTB ^= (1<<PB1);
		_delay_ms(1000);


	}
}
ISR(TIMER1_COMPA_vect)
{
	static uint16_t licznik = 0 ;
	if(licznik == czas)
	{
	PORTD &=~ (1<<PRZEK);  // WYŁACZENIE PRZEKAZNIKA
	TCNT2 = 0 ;
	TCCR2 |= (1<<CS22) | (1<<CS21) | (1<<CS20);	 // WŁACZENIE TIMERA I PRESKALER 1024
	licznik = 0 ;
	PORTB |= (1<<IGBT);    	// WŁACZENIE TRANZYSTORA IGBT
	}
	licznik ++ ;
}
ISR(TIMER2_COMP_vect)
{
	PORTB &=~ (1<<IGBT);	// WYŁACZENIE IGBT
	TCCR2 &=~ (1<<CS22) | (1<<CS21) | (1<<CS20);  // WYŁACZENNIE TIMREA
	TCNT2 = 0 ;
	PORTD |= (1<<PRZEK);  // WŁACZENIE PRZEKAZNIKA
}

